/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.SaleDao;
import java.util.List;
import model.ReportSale;

/**
 *
 * @author acer
 */
public class ReportService {
    
    public List<ReportSale> getreportSaleByDay(){
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }
    
     public List<ReportSale> getreportSaleByMouth(int year){
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }
    
}
